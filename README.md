# **Static Website Build System (SWBS)** #

## About ##
This project is used for developing simple static websites.
Languages used for development:
- Pug https://pugjs.org/api/getting-started.html.
- Sass https://sass-lang.com/.
**I might add some other language support such as js or php eventually.**

## Dependinces ##
- nodejs https://nodejs.org/en/.
- npm https://www.npmjs.com/.

## Usage ##
1. Clone the repo ```git clone https://gitlab.com/justinac/swbs```.
2. Change dir into folder ```swbs```.
**If you are not on macOS add ```--no-optional``` at the end of following command.**
3. Install the nodejs packages ```npm install```.
4. Run command ```gulp```.
5. Edit files in devel.

**This project is under the MIT License https://gitlab.com/justinac/swbs/blob/master/LICENSE.**