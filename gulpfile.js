// Package handles.
var gulp        = require("gulp");
var pug         = require("gulp-pug");
var sass        = require("gulp-sass");
var watch       = require("gulp-watch");
var browserSync = require("browser-sync").create();

// The compiler used for converting ".sass" into ".css"
sass.compiler = require("node-sass");

// Convert ".pug" code into ".html"
gulp.task("html", function() {
    return gulp.src("devel/pug/**/*.pug")
        .pipe(pug())
        .pipe(gulp.dest("build/"));
});

// Convert ".sass" code into ".css"
gulp.task("css", function() {
    return gulp.src("devel/sass/**/*.sass")
        .pipe(sass())
        .pipe(gulp.dest("build/css"));
});

// Refresh server.
gulp.task("server", function() {
    browserSync.init({
        server:  {
            baseDir: "./build/"
        }
    });
});

// Check run-time changes in ".pug" files.
gulp.task("watch:pug", function() {
    return watch("devel/pug/**/*.pug")
        .pipe(pug())
        .pipe(gulp.dest("build/"))
        .pipe(browserSync.stream());
});

// Check run-time changes in ".sass" files.
gulp.task("watch:sass", function() {
    return watch("devel/sass/**/*.sass")
        .pipe(sass())
        .pipe(gulp.dest("build/css"))
        .pipe(browserSync.stream());
});

// Run the guild system functions.
gulp.task("default", gulp.parallel("html", "css", "watch:pug", "watch:sass", "server"));
